const btns = document.querySelectorAll('.btn');
const changeBtn = document.querySelector('#changeBtn');
const body = document.querySelector('body');


if (!localStorage.getItem('bgcolor')) {
    setStyles();
} else {    
    getStyles();
}

document.addEventListener('keydown', (ev) => {

    btns.forEach(item => {
        if (localStorage.getItem('btncolor') == 'red') {
            item.style.backgroundColor = 'red';
        } else {
            item.style.backgroundColor = 'black';
        }
        
        if (ev.key.toLowerCase() == item.innerText.toLowerCase()) {
            item.style.backgroundColor = 'blue';
        }
    })
});

changeBtn.addEventListener('click', () => {
    
    setStyles();
    getStyles();

    changeBtn.blur();
})

// отримання стилів з localStorage
function getStyles() {
    let currentColor = localStorage.getItem('bgcolor');
    body.style.backgroundColor = currentColor;
    let btnCurrentColor = localStorage.getItem('btncolor');
    btns.forEach(item => {
        item.style.backgroundColor = btnCurrentColor;
    })
}

// встановленн стилів в localStorage
function setStyles() {
    if (body.style.backgroundColor == 'white') {
        localStorage.setItem('bgcolor', 'yellow');
        localStorage.setItem('btncolor', 'black');
    } else {
        localStorage.setItem('bgcolor', 'white');
        localStorage.setItem('btncolor', 'red');
    }   
}

getStyles();
